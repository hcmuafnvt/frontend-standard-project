module.exports = {
   development: {
      options: {
         compress: false,
         yuicompress: true,
         optimization: 2
      },
      files: {
         'assets/css/style.css': 'assets/less/style.less'
      }
   }
};
