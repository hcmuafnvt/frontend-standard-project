module.exports = {
   copyJS: {
      expand: true,
      cwd: 'bower_components/',
      src: [
         'jquery/dist/jquery.min.js'
      ],
      dest: 'assets/js/libs',
      flatten: true,
      filter: 'isFile'
   }
};
