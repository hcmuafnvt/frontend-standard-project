module.exports = {
   options: {
      mangle: true,
      compress: false,
      beautify: false,
      drop_console: true,
      preserveComments: false
   },
   files: {
      src: [
         //===libs===

         //===modules===

         //===pages===
      ],
      dest: 'fileName'
   }
};
