module.exports = {
   options: {
      "browser": true,
      "devel": true,
      "bitwise": true,
      "camelcase": false,
      "curly": true,
      "eqeqeq": true,
      "immed": true,
      "indent": 4,
      "latedef": true,
      "newcap": true,
      "noarg": true,
      "undef": true,
      "unused": true,
      "strict": true,
      "trailing": true,
      "nonew": true,
      "globals": {
         "jquery": true,
         "Modernizr": true,
         "Detectizr": true,
         "jQuery": true,
         "Site": true,
         "window": true,
         "$": true,
         "require": true,
         "define": true,
         "console": true
      },
      "ignores": ['elm']
   },
   src: ['assets/js/**/*.js', '!assets/js/libs/*.js']
};
