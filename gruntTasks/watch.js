module.exports = {
   less: {
      options: {
         livereload: true
      },
      files: ['assets/less/**/*.less'],
      tasks: ['less', 'cssmin']
   },
   scripts: {
      options: {
         livereload: true
      },
      files: ['assets/js/**/*.js', '!assets/js/libs/*.js'],
      tasks: ['jshint', 'requirejs']
   }
};
