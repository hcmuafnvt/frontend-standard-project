module.exports = {
   compile: {
      options: {
         baseUrl: "assets/js",
         mainConfigFile: "assets/js/main.js",
         dir: "assets/js-built",
         removeCombined: true,
         findNestedDependencies: true,
         optimize: "uglify",
         modules: [
            {
               name: 'main'
            },
            {
               name: 'pages/home.page',
               exclude: ['main']
            }
         ]
      }
   }
};
