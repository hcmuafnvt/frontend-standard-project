(function() {
   'use strict';

   require.config({
      paths: {
         jQuery: 'libs/jQuery.min'
      },
      shim: {
         jQuery: {
            exports: '$'
         }
      }
   });

   require(['modules/common.module'], function(common) {
      common.print();
   });
})();

