define([], function() {
   'use strict';

   var instance = {};

   instance.print = function() {
      console.log('loaded common module');
   };

   return instance;
});
